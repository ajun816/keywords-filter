﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AJ.KeywordFilter.Helper
{
    /// <summary>
    /// 关键词json配置文件模型
    /// </summary>
    public class KeywordConfig
    {
        /// <summary>
        /// 非法词语
        /// </summary>
        public List<string> IllegalKeywords { get; set; } = new List<string>();

        /// <summary>
        /// 非法网址
        /// </summary>
        public List<string> IllegalUrls { get; set; } = new List<string>();
    }
}