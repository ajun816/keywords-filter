﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolGood.Words;

namespace AJ.KeywordFilter.Helper
{
    public class KeywordCheckValidator : IKeywordCheckValidator
    {
        public ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is string v)
            {
                if (!String.IsNullOrEmpty(v))
                {
                    var obj = KeywordProvider.Instance;
                    var o = obj.IllegalWordsSearch;
                    o.ContainsAny(v);
                    if (KeywordProvider.Instance.IllegalWordsSearch.ContainsAny(v))
                    {
                        return new ValidationResult("存在敏感词", new[] { validationContext.MemberName });
                    }
                    // 检查拼音
                    if (KeywordProvider.Instance.IllegalWordsSearch.ContainsAny(WordsHelper.GetPinyin(v)))
                    {
                        return new ValidationResult("存在敏感词", new[] { validationContext.MemberName });
                    }
                    // todo:其他变种
                }
            }
            return ValidationResult.Success;
        }
    }
}