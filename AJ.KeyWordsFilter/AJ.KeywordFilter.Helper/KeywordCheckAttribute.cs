﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.DependencyInjection;

namespace AJ.KeywordFilter.Helper
{
    /// <summary>
    /// 敏感词检查的特性，一匹配就抛异常
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class KeywordCheckAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return validationContext.GetService<IKeywordCheckValidator>().IsValid(value, validationContext);
        }
    }
}