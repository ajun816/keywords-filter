﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AJ.KeywordFilter.Helper
{
    public static class KeywordExtension
    {
        public static void AddKeywordSetUp(this IServiceCollection services)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location))
                .AddJsonFile("IllegalUrls.json", optional: false, reloadOnChange: true)//配置热重载
                .AddJsonFile("IllegalKeywords.json", optional: false, reloadOnChange: true);//配置热重载
            //创建配置根对象
            var configurationRoot = builder.Build();
            var o = configurationRoot.GetConnectionString("IllegalKeywords");
            var list = configurationRoot.GetSection("IllegalKeywords");
            var lists = configurationRoot.GetSection("IllegalKeywords").Get<List<string>>();
            KeywordProvider.Instance.SetKeys(configurationRoot.GetSection("IllegalKeywords").Get<List<string>>());
            ChangeToken.OnChange(() => configurationRoot.GetReloadToken(), () =>
            {
                // 敏感词重载
                KeywordProvider.Instance.SetKeys(configurationRoot.GetSection("IllegalKeywords").Get<List<string>>());
            });
            services.AddSingleton<IKeywordCheckValidator, KeywordCheckValidator>()
                .AddSingleton<IKeywordReplaceValidator, KeywordReplaceValidator>();
        }
    }
}