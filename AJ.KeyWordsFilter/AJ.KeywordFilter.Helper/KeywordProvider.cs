﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolGood.Words;

namespace AJ.KeywordFilter.Helper
{
    public sealed class KeywordProvider
    {
        private static readonly Lazy<KeywordProvider>
           lazy =
               new Lazy<KeywordProvider>
                   (() => new KeywordProvider());

        public static KeywordProvider Instance
        { get { return lazy.Value; } }

        private KeywordProvider()
        {
            IllegalWordsSearch = new IllegalWordsSearch();
        }

        public readonly IllegalWordsSearch IllegalWordsSearch;

        public void SetKeys(List<string> keys)
        {
            if (keys != null && keys.Any())
            {
                var allKeys = new List<string>();
                foreach (var k in keys)
                {
                    allKeys.Add(k); // 增加词汇
                    allKeys.Add(WordsHelper.ToTraditionalChinese(k)); // 增加繁体
                    allKeys.Add(WordsHelper.GetPinyin(k)); // 增加拼音
                }
                IllegalWordsSearch.SetKeywords(allKeys);
            }
        }
    }
}