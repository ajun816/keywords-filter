﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AJ.KeywordFilter.Helper
{
    public class KeywordReplaceValidator : IKeywordReplaceValidator
    {
        public void Replace(object value, ValidationContext validationContext)
        {
            if (value is string v)
            {
                if (!String.IsNullOrEmpty(v))
                {
                    v = KeywordProvider.Instance.IllegalWordsSearch.Replace(v);
                    SetPropertyByName(validationContext.ObjectInstance, validationContext.MemberName, v);
                }
            }
        }

        private static bool SetPropertyByName(Object obj, string name, Object value)
        {
            var type = obj.GetType();
            var prop = type.GetProperty(name, BindingFlags.Public | BindingFlags.Instance);
            if (null == prop || !prop.CanWrite) return false;
            prop.SetValue(obj, value, null);
            return true;
        }
    }
}