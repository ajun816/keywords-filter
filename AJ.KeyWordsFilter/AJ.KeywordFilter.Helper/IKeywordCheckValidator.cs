﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AJ.KeywordFilter.Helper
{
    /// <summary>
    /// 关键词检查
    /// </summary>
    public interface IKeywordCheckValidator
    {
        ValidationResult IsValid(object value, ValidationContext validationContext);
    }
}