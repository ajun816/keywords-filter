﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AJ.KeywordFilter.Helper
{
    /// <summary>
    /// 关键词替换
    /// </summary>
    public interface IKeywordReplaceValidator
    {
        void Replace(object value, ValidationContext validationContext);
    }
}