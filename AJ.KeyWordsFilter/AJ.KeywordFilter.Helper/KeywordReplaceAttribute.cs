﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.DependencyInjection;

namespace AJ.KeywordFilter.Helper
{
    /// <summary>
    /// 敏感词替换的特性,一匹配就替换
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class KeywordReplaceAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            validationContext.GetService<IKeywordReplaceValidator>().Replace(value, validationContext);
            return ValidationResult.Success;
        }
    }
}