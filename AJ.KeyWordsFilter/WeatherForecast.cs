using AJ.KeywordFilter.Helper;

namespace AJ.KeyWordsFilter
{
    public class WeatherForecast
    {
        //public DateOnly Date { get; set; }

        //public int TemperatureC { get; set; }

        //public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        [KeywordCheck]
        public string? Summary { get; set; }
    }
}